<?php

require_once 'db.php';

// Successful checkin message default
$productAddedSuccess = false;


if (!empty($_POST)){

$username = $_POST['user-name'];
$rating = $_POST['rating'];
$review = $_POST['review'];

// Submit to database
$sql = "INSERT INTO checkin (name, rating, review) VALUES (:username, :rating, :review)";
$stmt = $dbh->prepare($sql);
$stmt->execute([
'username' => $username,
'rating' => $rating,
'review' => $review,
]);

//Output
echo "# of rows affected" . $stmt->rowCount();

// Successful checkin message
$productAddedSuccess = true;

}

?>

<?php


if (!isset($_GET['id'])) {
    die('Missing id in the URL');
}

if (
    isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
    strcasecmp($_SERVER['HTTP_X_REQUESTED_WITH'], 'xmlhttprequest') === 0
) {
    $id = $_GET['id'];

    $stmt = $dbh->prepare('SELECT id, name, rating, review, posted FROM checkin WHERE product_id = :id'
    );

    $stmt->execute(['id'=>$id]);

    $checkins = $stmt->fetchAll(PDO::FETCH_ASSOC);
    echo json_encode($checkins);
    die();
}

?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <!-- Axios -->
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <title>Initech</title>

    <style>
        .star-rating {
            background-color: grey;
            width: 200px;
            height: 25px;
            margin-left: 10px;
            display: inline-block;
        }

        .star-rating div {
            height: 100%;
            background-color: orange;
        }

        #starCodes {
            font-size: 28px;
            color: orange;
        }

        h3 {
            font-size: 25px;
        }
    </style>
</head>

<body class="p-4">
<!-- Carousel-->
<div class="container">
    <div class="card p-4">
        <div class="row">
            <div class="col-md-6 col-sm-12 py-8">
                <div id="catSlide" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active"><img class="img-fluid d-block w-100" src="cat1.jpg" alt="first cat"></div>
                        <div class="carousel-item"><img class="img-fluid d-block w-100" src="cat2.png" alt="second cat"></div>
                        <div class="carousel-item"><img class="img-fluid d-block w-100" src="cat3.png" alt="third cat"></div>
                        <div class="carousel-item"><img class="img-fluid d-block w-100" src="cat4.png" alt="fourth cat"></div>
                    </div>
                    <a class="carousel-control-prev" href="#catSlide" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href='#catSlide' role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>



            <div class="col-md-6 col-sm-12">
                <h1>Lorem Ipsum</h1>
                <?php if ($productAddedSuccess): ?>
                    <p class="alert alert-success">You checked in successfully!</p>
                <?php endif; ?>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                    ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                    voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                    non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>

<!-- Modal: Check In Section -->

                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#checkInModal" data-whatever="@checkIn">Check In</button>

                <div class="modal fade" id="checkInModal" tabindex="-1" role="dialog" aria-labelledby="checkInModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="checkInLabel">Leave a review and check in!</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="" method="post">
                                    <div class="form-group">
                                        <label for="name" class="col-form-label">Name:</label>
                                        <input type="text" class="form-control" name="user-name" id="name">
                                    </div>
                                    <div class="form-group">
                                        <label for="star-rating" class="col-form-label">Rating from 1-5</label>
                                        <input type="text" class="form-control" name="rating" id="star-rating">
                                    </div>
                                    <div class="form-group">
                                        <label for="user-review" class="col-form-label">Review:</label>
                                        <textarea class="form-control" name="review" id="user-review"></textarea>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-success" id="submit">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>

<!-- Additional Info -->

    <h2 class="my-4">Additional Information</h2>
    <div class="card p-4 my-4">
        <table class="table">
            <tbody>
            <tr>
                <th>Average Rating</th>
                <td id ="starCodes">&#9733;&#9733;&#9733;&#9733;&#9734;</td>
            </tr>
            <tr>
                <th>Another Statistic</th>
                <td>78/100</td>
            </tr>
            <tr>
                <th>Yet Another Statistic</th>
                <td>Something useful</td>
            </tr>
            </tbody>
        </table>
    </div>
    <h2 class="my-4">Recent Checkins</h2>

    <div id="checkins"></div>
</div>

<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper)
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
-->

<!-- Option 2: jQuery, Popper.js, and Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>

<script>
    // Force Ajax header
    axios.defaults.headers.common['X-Requested-with'] = 'XMLHttpRequest';

    axios.get('/project/initech/index.php?id=1')
        .then(function (response) {
            console.log(response);
            if (response.data.length === 0) {
                $('<div>').text('No checkins available').appendTo('#checkins');
                return;
            }

            // loop for each response
            // create a DOM object
            // Build each data item into DOM object
            // Append our DOM object to #checkins
            for(let i = 0; i < response.data.length; i++) {
                let checkinElement = $('<div>').addClass('card p-4 my-4');

                let h3 = $('<h3>').text(response.data[i].name).appendTo(checkinElement);

                let starRating = $('<div>').addClass('star-rating');

                let checkinRating = response.data[i].rating * 20;

                $('<div>').css('width', checkinRating + '%').appendTo(starRating);

                starRating.appendTo(h3);

                $('<p>').text(response.data[i].review).appendTo(checkinElement);

                let p = $('<p>').text(response.data[i].posted).appendTo(checkinElement);

                $('#checkins').append(checkinElement);
            }
        })
        .catch(function (error) {
            console.log(error);
        });
</script>
</body>
</html>
