<?php

require_once 'db.php';

// Successful checkin message default
$productAddedSuccess = false;


if (!empty($_POST)){

    $username = $_POST['user-name'];
    $rating = $_POST['rating'];
    $review = $_POST['review'];

// Submit to database
    $sql = "INSERT INTO checkin (user_name, rating, review) VALUES (:username, :rating, :review)";
    $stmt = $dbh->prepare($sql);
    $stmt->execute([
        'username' => $username,
        'rating' => $rating,
        'review' => $review,
    ]);


// Successful checkin message
    $productAddedSuccess = true;

}

?>

<?php


if (!isset($_GET['id'])) {
    /*die('Missing id in the URL');*/
}

if (
    isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
    strcasecmp($_SERVER['HTTP_X_REQUESTED_WITH'], 'xmlhttprequest') === 0
) {
    $id = $_GET['id'];

    $stmt = $dbh->prepare('SELECT id, user_name, rating, review, posted FROM checkin WHERE animal_id = :id');

    $stmt->execute(['id'=>$id]);

    $checkins = $stmt->fetchAll(PDO::FETCH_ASSOC);
    echo json_encode($checkins);
    die();
}

?>


<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">

    <link rel="stylesheet" href="style.css">

    <title>Weird & Wonderful</title>
</head>
<body>
<div class="wrapper d-flex align-items-stretch">
<!-- Side bar -->
    <nav id="sidebar">
        <div class="p-4 pt-5">
            <a href="#" class="img logo rounded-circle mb-5" style="background-image: url(img/logo.jpg);"></a>
            <ul class="list-unstyled components mb-5">
                <li>
                    <a href="#home">Home</a>
                </li>
                <li class="active">
                    <a href="#animalMenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Pets</a>
                    <ul class="collapse list-unstyled" id="animalMenu">
                        <li>
                            <a href="#hedghog">African Pygmy Hedgehog</a>
                        </li>
                        <li>
                            <a href="#alligator">Alligator</a>
                        </li>
                        <li>
                            <a href="#tiger">Bengal Tiger</a>
                        </li>
                        <li>
                            <a href="#dragon">Chinese Water Dragon</a>
                        </li>
                        <li>
                            <a href="#fox">Fennec Fox</a>
                        </li>
                        <li>
                            <a href="#snake">Honduran Milk Snake</a>
                        </li>
                        <li>
                            <a href="#stick">Indian Stick Insect</a>
                        </li>
                        <li>
                            <a href="#panda">Red Panda</a>
                        </li>
                        <li>
                            <a href="#alpaca">Suri Alpaca</a>
                        </li>
                        <li>
                            <a href="#mastiff">Tibetan Mastiff</a>
                        </li>
                    </ul>
                </li>
            </ul>
                <div class="footer">
                    <p><!-- Copyright automatically sets current year -->
                        Copyright &copy;<script>document.write(new Date().getFullYear());</script><br> All rights reserved
                    </p>
                </div>

        </div>
    </nav>

<!-- Page Content  -->
    <div id="content" class="p-4 p-md-5">

        <!--<nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="product-list.php">Pets</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <div class="navbar-nav">
                    <a class="nav-link" href="product-list.php">
                        Home <span class="sr-only">(current)</span></a>
                </div>
            </div>
        </nav>-->

        <h2 class="mb-4">Weird & Wonderful Pets</h2>

        <div class="container">
            <div id="myBorder" class="card p-6">
                <div class="row">
                    <div class="col-md-6 col-sm-12 py-8">
                        <img src="img/alligator.jpg" alt="alligator" class="img-fluid">
                    </div>

                    <div id="parigraphry" class="col-md-6 col-sm-12">
                        <h1>Lorem Ipsum</h1>
                        <?php if ($productAddedSuccess): ?>
                            <p class="alert alert-success">You checked in successfully!</p>
                        <?php endif; ?>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                            voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                            non.
                        </p>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#checkInModal" data-whatever="@checkIn">Review</button>
                    </div>
                </div>
            </div>

        <?php if ($productAddedSuccess): ?>
            <p class="alert alert-success">You checked in successfully!</p>
        <?php endif; ?>

<!-- Modal: Check In Section -->
        <div class="modal fade" id="checkInModal" tabindex="-1" role="dialog" aria-labelledby="checkInModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="checkInLabel">Leave a review and check in!</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post">
                            <div class="form-group">
                                <label for="name" class="col-form-label">Name:</label>
                                <input type="text" class="form-control" name="user-name" id="name">
                            </div>
                            <div class="form-group">
                                <label for="star-rating" class="col-form-label">Rating from 1-5</label>
                                <input type="text" class="form-control" name="rating" id="star-rating">
                            </div>
                            <div class="form-group">
                                <label for="user-review" class="col-form-label">Review:</label>
                                <textarea class="form-control" name="review" id="user-review"></textarea>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-success" id="submit">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script>
            if ( window.history.replaceState ) {
                window.history.replaceState( null, null, window.location.href );
            }
        </script>

        <div id="checkins"></div>

        <!--<table class="table">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Title</th>
            </tr>
            </thead>
            <tbody>
            <?php /*foreach ($products as $product): */?>
            <tr>
                <td><a href="product.php?id=<?/*= $product->id*/?>"><?/*= $product->id */?></a></td>
                <td><a href="product.php?id=<?/*= $product->id*/?>"><?/*= $product->title */?></a></td>
                <?php /*endforeach; */?>
            </tbody>
        </table>-->

            <!--<div class="container">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <a class="navbar-brand" href="product-list.php">Cats</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <div class="navbar-nav">
                            <a class="nav-link" href="product-list.php">
                                Home <span class="sr-only">(current)</span></a>
                        </div>
                    </div>
                </nav>
                <h1>Products!</h1>
                <form>
                    <input type="text" name="search" value="<?/*= $searchTerm */?>">
                    <input type="submit">
                </form>
                <?php
/*                include 'template_parts/product_table.php'
                */?>
            </div>-->


<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper)
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
-->

<!-- Option 2: jQuery, Popper.js, and Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>

<!-- Axios -->
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

<script>
// Force Ajax header
    axios.defaults.headers.common['X-Requested-with'] = 'XMLHttpRequest';

    axios.get('/project/index.php?id=2')
        .then(function (response) {
            console.log(response);
            if (response.data.length === 0) {
                $('<div>').text('No checkins available').appendTo('#checkins');
                return;
            }

            // loop for each response
            // create a DOM object
            // Build each data item into DOM object
            // Append our DOM object to #checkins
            for(let i = 0; i < response.data.length; i++) {
                let checkinElement = $('<div>').addClass('card p-4 my-4');

                let h3 = $('<h3>').text(response.data[i].user_name).appendTo(checkinElement);

                let starRating = $('<div>').addClass('star-rating');

                let checkinRating = response.data[i].rating * 20;

                $('<div>').css('width', checkinRating + '%').appendTo(starRating);

                starRating.appendTo(h3);

                $('<p>').text(response.data[i].review).appendTo(checkinElement);

                let p = $('<p>').text(response.data[i].posted).appendTo(checkinElement);

                $('#checkins').append(checkinElement);
            }
        })
        .catch(function (error) {
            console.log(error);
        });
</script>

</body>
</html>