<?php

use App\Hydrator\EntityHydrator;

require_once '../src/setup.php';

$hydrator = new EntityHydrator();

$checkInData = [
    'product_id' => filter_var($_POST['product_id'], FILTER_VALIDATE_INT),
    'name' => strip_tags($_POST['name']),
    'rating' => filter_var($_POST['rating'], FILTER_VALIDATE_INT),
    'review' => strip_tags($_POST['review']),
];

if (empty($checkInData['product_id'])) {
    header('Location: product-list.php');
    exit;
}

if (empty($checkInData['name']) || empty($checkInData['rating'] || empty($checkInData['review']))) {
    header('Location: product.php?id=' . $checkInData['product_id'] . '&message=error');
    exit;
}

$postedCheckIn = $hydrator->hydrateCheckIn($checkInData);

$checkIn = $dbProvider->createCheckIn($postedCheckIn);

header('Location: product.php?id=' . $_POST['product_id'] . '&message=success');
