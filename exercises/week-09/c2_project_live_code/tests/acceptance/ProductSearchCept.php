<?php

$I = new AcceptanceTester($scenario);
$I->wantTo('perform actions and see result');

$I->amOnPage('/product-list.php');
$I->see('Products!', 'div.container h1');

$I->wait(2);

$I->fillField('search', 'mac');
$I->click('Search', 'div.container form');

$I->waitForText('1 results found');

$I->wait(5);

