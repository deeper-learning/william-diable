<?php

$I = new AcceptanceTester($scenario);
$I->wantTo('perform actions and see result');

$I->amOnPage('/product-list.php');
$I->see('Products!', 'div.container h1');

$I->wait(5);
