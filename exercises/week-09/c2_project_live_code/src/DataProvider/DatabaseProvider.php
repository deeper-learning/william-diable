<?php

namespace App\DataProvider;

use App\Entity\CheckIn;
use App\Entity\Product;
use App\Entity\User;
use App\Hydrator\EntityHydrator;
use App\Hydrator\UserHydrator;
use Monolog\Logger;
use PDO;
use PDOException;

class DatabaseProvider
{
    private PDO $dbh;
    private ProductDataProvider $productDataProvider;

    public function __construct(Logger $logger)
    {
        $username = 'root';
        $password = 'root';

        try {
            $this->dbh = new PDO(
                'mysql:dbname=project;host=mysql',
                $username,
                $password
            );

            $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        } catch (PDOException $e) {
            $logger->critical('Unable to establish Db connection', ['error' => $e->getMessage()]);

            die('Unable to establish a database connection');
        }

        $this->productDataProvider = new ProductDataProvider($this->dbh);
    }

    public function getProducts(string $searchTerm = ''): array
    {
        return $this->productDataProvider->getProducts($searchTerm);
    }

    public function getProduct(int $productId): Product
    {
        return $this->productDataProvider->getProduct($productId);
    }

    public function createProduct(Product $product): Product
    {
        return $this->productDataProvider->createProduct($product);
    }

    public function getCheckIn(int $checkInId): ?CheckIn
    {
        $stmt = $this->dbh->prepare(
            'SELECT id, product_id, name, rating, review, posted
            FROM checkin
            WHERE id = :id'
        );

        $stmt->execute(['id' => $checkInId]);

        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if (empty($result)) {
            return null;
        }

        $hydrator = new EntityHydrator();

        return $hydrator->hydrateCheckIn($result);
    }

    public function createCheckIn(CheckIn $checkIn)
    {
        $stmt = $this->dbh->prepare(
            'INSERT INTO checkin
            (name, rating, review, product_id)
            VALUES
            (:name, :rating, :review, :product_id)'
        );

        $stmt->execute([
            'name' => $checkIn->name,
            'rating' => $checkIn->rating,
            'review' => $checkIn->review,
            'product_id' => $checkIn->product_id,
        ]);

        $lastInsertId = $this->dbh->lastInsertId();
        $newCheckin = $this->getCheckIn($lastInsertId);

        return $newCheckin;
    }

    public function getUser(int $userId): ?User
    {
        $stmt = $this->dbh->prepare(
            'SELECT id, name, email_address, password
            FROM user
            WHERE id = :id'
        );

        $stmt->execute(['id' => $userId]);

        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if (empty($result)) {
            return null;
        }

        $hydrator = new UserHydrator();

        return $hydrator->hydrateUser($result);
    }

    public function createUser(User $user): User
    {
        $stmt = $this->dbh->prepare(
            'INSERT INTO user (name, email_address, password)
            VALUES (:name, :email_address, :password)'
        );

        $stmt->execute([
            'name' => $user->name,
            'email_address' => $user->emailAddress,
            'password' => $user->password,
        ]);

        $lastInsertId = $this->dbh->lastInsertId();
        $newUser = $this->getUser($lastInsertId);

        return $newUser;
    }
}
