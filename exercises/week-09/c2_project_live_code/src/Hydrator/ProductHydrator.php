<?php

namespace App\Hydrator;

use App\Entity\Product;

class ProductHydrator
{
    public function hydrateProduct(array $data): Product
    {
        $product = new Product();
        $product->id = $data['id'] ?? null;
        $product->title = $data['title'];
        $product->description = $data['description'];
        $product->imagePath = $data['image_path'];
        $product->averageRating = $data['average_rating'] ?? null;

        return $product;
    }
}
