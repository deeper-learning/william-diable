<?php

namespace App\Hydrator;

use App\Entity\Product;

class EntityHydrator
{
    private ProductHydrator $productHydrator;
    private CheckInHydrator $checkInHydrator;

    public function __construct()
    {
        $this->productHydrator = new ProductHydrator();
        $this->checkInHydrator = new CheckInHydrator();
    }

    public function hydrateProductWithCheckins(array $data): Product
    {
        $productData = [
            'id' => $data[0]['product_id'],
            'title' => $data[0]['title'],
            'description' => $data[0]['description'],
            'image_path' => $data[0]['image_path'],
            'average_rating' => $data[0]['average_rating'],
        ];

        $product = $this->productHydrator->hydrateProduct($productData);

        foreach ($data as $checkInData) {
            if ($checkInData['name'] !== null) {
                $checkIn = $this->checkInHydrator->hydrateCheckIn($checkInData);
                $product->addCheckin($checkIn);
            }
        }

        return $product;
    }
}
