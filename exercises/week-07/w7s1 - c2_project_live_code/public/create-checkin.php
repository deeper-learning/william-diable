<?php

use App\Hydrator\EntityHydrator;

require_once '../src/setup.php';

$hydrator = new EntityHydrator();

$checkInData = [
    'product_id' => $_POST['product_id'],
    'name' => $_POST['name'],
    'rating' => $_POST['rating'],
    'review' => $_POST['review'],
];

if (empty($checkInData['product_id'])) {
    header('Location: product-list.php');
}

if (empty($checkInData['name']) || empty($checkInData['rating'] || empty($checkInData['review']))) {
    header('Location: product.php?id=' . $checkInData['product_id'] . '&message=error');
}

$postedCheckIn = $hydrator->hydrateCheckIn($checkInData);

$checkIn = $dbProvider->createCheckIn($postedCheckIn);

header('Location: product.php?id=' . $_POST['product_id'] . '&message=success');
