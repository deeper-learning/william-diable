<?php

use App\Entity\Product;

require_once '../src/setup.php';

if (!empty($_POST['title']) && !empty($_POST['description'])) {
    $imageName = time() . '_' . $_FILES['image_path']['name'];

    move_uploaded_file($_FILES['image_path']['tmp_name'], 'uploads/' . $imageName);
    $formProduct = new Product();
    $formProduct->title = $_POST['title'];
    $formProduct->description = $_POST['description'];
    $formProduct->imagePath = $imageName;

    $product = $dbProvider->createProduct($formProduct);
    header('Location: product.php?id=' . $product->id);
    exit;
}

?>
<!doctype html>
<html lang="en">
<head>
    <?php include 'template_parts/header_includes.php' ?>
    <title>Create Product</title>
</head>
<body>
<div class="container">
    <div class="card p-4">
        <h1>Create Product</h1>
        <form method="post" enctype="multipart/form-data">
            <div class="col-md-6 col-sm-6 text-center">
                <label for="file">Image</label>
                <input type="file" accept="image/png, image/jpeg" name="image_path" id="image" class="form-control-file" aria-describedby="fileUpload">
                <small id="fileUpload" class="form-text text-muted">Images types should be PNG or JPEG</small>
            </div>
            <div class="col-md-6 col-sm-12">
                <label for="title">Title</label>
                <input class="form-control" name="title" id="title" placeholder="Title">
                <label for="description">Description</label>
                <textarea class="form-control" name="description" id="description" placeholder="Description" rows="10"></textarea>
                <button type="submit" class="btn btn-primary">Create</button>
            </div>
        </form>
    </div>
<?php include 'template_parts/footer_includes.php' ?>
</body>
</html>
