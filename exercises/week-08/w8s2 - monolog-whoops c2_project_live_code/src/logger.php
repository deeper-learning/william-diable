<?php

use Monolog\Handler\ChromePHPHandler;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Whoops\Handler\JsonResponseHandler;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;

// Set up Monolog
$logger = new Logger('DEEPer Starter');
$logPath = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'logs';
if (!is_dir($logPath) && !mkdir($logPath, 0775, true)) {
    throw new \RuntimeException(sprintf('Directory "%s" was not created', $logPath));
}

// Set up log handlers
$handlers = [
    new RotatingFileHandler($logPath . DIRECTORY_SEPARATOR . 'project.log', 5)
];

if (defined('APP_ENV') && APP_ENV === 'localhost') {
    $handlers[] = new ChromePHPHandler();
}

$logger->setHandlers($handlers);

// Log message prefix
if (defined('APP_ENV')) {
    $logger->pushProcessor(function ($record) {
        $record['message'] = APP_ENV . ': ' . $record['message'];
        return $record;
    });
}

// Set up Whoops
$whoops = new Run();

// Add the default Whoops handlers
if (Whoops\Util\Misc::isAjaxRequest()) {
    $whoops->prependHandler(new JsonResponseHandler());
}

if (defined('APP_ENV') && APP_ENV === 'localhost') {
    $pageHandler = new PrettyPageHandler();
    $pageHandler->setEditor('phpstorm');
    $whoops->prependHandler($pageHandler);
}

// Set up the monolog custom handler
$whoops->prependHandler(
    function ($exception, $inspector, $run) use ($logger) {

        /* @var $exception \Exception */
        $logger->log(
            Logger::ERROR,
            $exception->getMessage(),
            [
                'code' => $exception->getCode(),
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
                'trace' => $exception->getTraceAsString(),
            ]
        );
    }
);

$whoops->register();
