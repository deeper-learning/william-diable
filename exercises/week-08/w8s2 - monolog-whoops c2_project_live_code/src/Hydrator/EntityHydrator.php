<?php

namespace App\Hydrator;

use App\Entity\CheckIn;
use App\Entity\Product;

class EntityHydrator
{
    public function hydrateProduct(array $data): Product
    {
        $product = new Product();
        $product->id = $data['id'] ?? null;
        $product->title = $data['title'];
        $product->description = $data['description'];
        $product->imagePath = $data['image_path'];
        $product->averageRating = $data['average_rating'] ?? null;

        return $product;
    }

    public function hydrateCheckIn(array $data): CheckIn
    {
        $checkIn = new CheckIn();
        $checkIn->id = $data['id'] ?? null;
        $checkIn->product_id = $data['product_id'];
        $checkIn->name = $data['name'];
        $checkIn->rating = $data['rating'];
        $checkIn->review = $data['review'];
        $checkIn->posted = $data['posted'] ?? null;

        return $checkIn;
    }

    public function hydrateProductWithCheckins(array $data): Product
    {
        $productData = [
            'id' => $data[0]['product_id'],
            'title' => $data[0]['title'],
            'description' => $data[0]['description'],
            'image_path' => $data[0]['image_path'],
            'average_rating' => $data[0]['average_rating'],
        ];

        $product = $this->hydrateProduct($productData);

        foreach ($data as $checkInData) {
            if ($checkInData['name'] !== null) {
                $checkIn = $this->hydrateCheckIn($checkInData);
                $product->addCheckin($checkIn);
            }
        }

        return $product;
    }
}
