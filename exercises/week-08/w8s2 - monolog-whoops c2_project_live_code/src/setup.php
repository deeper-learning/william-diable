<?php

define('APP_ENV', 'localhost');

use \App\DataProvider\DatabaseProvider;

require_once '../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

require_once __DIR__ . DIRECTORY_SEPARATOR . 'logger.php';

$dbProvider = new DatabaseProvider($logger);
