<?php

namespace App\DataProvider;

use App\Entity\CheckIn;
use App\Entity\Product;
use App\Entity\User;
use App\Hydrator\EntityHydrator;
use App\Hydrator\UserHydrator;
use Monolog\Logger;
use PDO;
use PDOException;

class DatabaseProvider
{
    private PDO $dbh;

    public function __construct(Logger $logger)
    {
        $username = 'root';
        $password = 'root';

        try {
            $this->dbh = new PDO(
                'mysql:dbname=project;host=mysql',
                $username,
                $password
            );

            $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        } catch (PDOException $e) {
            $logger->critical('Unable to establish Db connection', ['error' => $e->getMessage()]);

            die('Unable to establish a database connection');
        }
    }

    public function getProducts(string $searchTerm = ''): array
    {
        $stmt = $this->dbh->prepare('SELECT id, title FROM product WHERE title LIKE :searchTerm');
        $stmt->execute([
            'searchTerm' => '%' . $searchTerm . '%'
        ]);

        /** @var Product[] $products */
        return $stmt->fetchAll(PDO::FETCH_CLASS, Product::class);
    }

    public function getProduct(int $productId): Product
    {
        $stmt = $this->dbh->prepare(
            'SELECT
                p.id AS product_id, p.title, p.description, p.image_path,
                c.id, c.name, c.rating, c.review, c.posted,
                (
                    SELECT AVG(rating)
                    FROM checkin
                    WHERE product_id = p.id
                ) AS average_rating
            FROM product p
            LEFT JOIN checkin AS c ON c.product_id = p.id
            WHERE p.id = :id'
        );
        $stmt->execute([
            'id' => $productId
        ]);

        $productData = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $hydrator = new EntityHydrator();
        return $hydrator->hydrateProductWithCheckins($productData);
    }

    public function getCheckIn(int $checkInId): ?CheckIn
    {
        $stmt = $this->dbh->prepare(
            'SELECT id, product_id, name, rating, review, posted
            FROM checkin
            WHERE id = :id'
        );

        $stmt->execute(['id' => $checkInId]);

        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if (empty($result)) {
            return null;
        }

        $hydrator = new EntityHydrator();

        return $hydrator->hydrateCheckIn($result);
    }

    public function createProduct(Product $product)
    {
        $stmt = $this->dbh->prepare(
          'INSERT INTO product (title, description, image_path)
            VALUES (:title, :description, :imagePath)'
        );

        $stmt->execute([
            'title' => $product->title,
            'description' => $product->description,
            'imagePath' => $product->imagePath,
        ]);

        $lastInsertId = $this->dbh->lastInsertId();
        $newProduct = $this->getProduct($lastInsertId);
        return $newProduct;
    }

    public function createCheckIn(CheckIn $checkIn)
    {
        $stmt = $this->dbh->prepare(
            'INSERT INTO checkin
            (name, rating, review, product_id)
            VALUES
            (:name, :rating, :review, :product_id)'
        );

        $stmt->execute([
            'name' => $checkIn->name,
            'rating' => $checkIn->rating,
            'review' => $checkIn->review,
            'product_id' => $checkIn->product_id,
        ]);

        $lastInsertId = $this->dbh->lastInsertId();
        $newCheckin = $this->getCheckIn($lastInsertId);

        return $newCheckin;
    }

    public function getUser(int $userId): ?User
    {
        $stmt = $this->dbh->prepare(
            'SELECT id, name, email_address, password
            FROM user
            WHERE id = :id'
        );

        $stmt->execute(['id' => $userId]);

        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if (empty($result)) {
            return null;
        }

        $hydrator = new UserHydrator();

        return $hydrator->hydrateUser($result);
    }

    public function createUser(User $user): User
    {
        $stmt = $this->dbh->prepare(
            'INSERT INTO user (name, email_address, password)
            VALUES (:name, :email_address, :password)'
        );

        $stmt->execute([
            'name' => $user->name,
            'email_address' => $user->emailAddress,
            'password' => $user->password,
        ]);

        $lastInsertId = $this->dbh->lastInsertId();
        $newUser = $this->getUser($lastInsertId);

        return $newUser;
    }
}
