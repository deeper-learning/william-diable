<?php

require_once '../src/setup.php';

if (isset($_POST['name'], $_POST['email'], $_POST['password'], $_POST['confirmPassword'])) {
    if ($_POST['password'] === $_POST['confirmPassword']) {
        $userHydrator = new \App\Hydrator\UserHydrator();
        $formUser = $userHydrator->hydrateUser([
            'name' => $_POST['name'],
            'email_address' => $_POST['email'],
            'password' => password_hash($_POST['password'], PASSWORD_DEFAULT)
        ]);

        $user = $dbProvider->createUser($formUser);
    }
}

?>
<!doctype html>
<html lang="en">
<head>
    <?php include 'template_parts/header_includes.php'; ?>
    <title>Register</title>
</head>
<body class="p-4">
<div class="container">
    <?php include 'template_parts/nav.php'; ?>
    <h1>Register!</h1>
    <form method="post">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" class="form-control">
                </div>
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" name="email" id="email" class="form-control">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password" class="form-control">
                </div>
                <div class="form-group">
                    <label for="confirm">Confirm Password</label>
                    <input type="password" name="confirmPassword" id="confirm" class="form-control">
                </div>
            </div>
            <div class="col-md-12 text-center">
                <input type="submit" class="btn btn-primary">
            </div>
        </div>
    </form>
</div>
<?php include 'template_parts/footer_includes.php' ?>
</body>
</html>
