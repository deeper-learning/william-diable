<?php

require_once 'setup.php';

if (!isset($_GET['id'])) {
    die('Missing id in the URL');
}

$id = $_GET['id'];

$product = $dbProvider->getProduct($id);

if (!$product) {
    header('Location: 404.php');
    exit;
}

?>
<!doctype html>
<html lang="en">
<head>
    <?php include 'template_parts/header_includes.php' ?>
    <title>Products</title>

    <style>
        .star-rating {
            background-color: grey;
            width: 200px;
            height: 30px;
            display: inline-block;
        }

        .star-rating div {
            height: 100%;
            background-color: yellow;
        }
    </style>
</head>
<body class="p-4">
<div class="container">
    <?php include 'template_parts/nav.php'; ?>
    <div class="card p-4">

        <div class="row">
            <div class="col-md-6 col-sm-12 text-center">
                <img src="uploads/<?= $product->imagePath ?>" alt="cat" class="img-fluid">
                <div class="row py-2">
                    <div class="col-4"><img src="http://placekitten.com/200/150" alt="cat" class="img-fluid"></div>
                    <div class="col-4"><img src="http://placekitten.com/200/150" alt="cat" class="img-fluid"></div>
                    <div class="col-4"><img src="http://placekitten.com/200/150" alt="cat" class="img-fluid"></div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <h1><?= $product->title; ?></h1>
                <p>
                    <?= strip_tags($product->description) ?>
                </p>

                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#checkInModal">
                    Check In
                </button>
            </div>
        </div>
    </div>
    <h2 class="my-4">Additional Information</h2>
    <div class="card p-4 my-4">
        <table class="table">
            <tbody>
            <tr>
                <th>Average Rating</th>
                <td><?= $product->averageRating; ?><img src="assets/4-star-image.JPG" alt="star rating" style="width: 271px;height: 52px;"></td>
            </tr>
            <tr>
                <th>Another Statistic</th>
                <td>78/100</td>
            </tr>
            <tr>
                <th>Yet Another Statistic</th>
                <td>Something useful</td>
            </tr>
            </tbody>
        </table>
    </div>
    <h2 class="my-4">Recent Checkins</h2>

    <div id="checkins">
        <?php foreach ($product->getCheckins() as $checkIn): ?>
            <div class="card p-4 my-4">
                <h3>
                    <?= $checkIn->name ?>
                    <div class="star-rating"><div style="width: <?= $checkIn->rating * 20 ?>%;"></div></div>
                </h3>

                <p>
                    <?= $checkIn->review ?>
                </p>
            </div>
        <?php endforeach; ?>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="checkInModal" tabindex="-1" aria-labelledby="checkInModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="checkInModalLabel">Check In</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="create-checkin.php" method="post">
                    <div class="modal-body">
                        <input type="hidden" name="product_id" value="<?= $_GET['id'] ?>">
                        <div class="form-group">
                            <label for="name">Your Name</label>
                            <input type="text" class="form-control" name="name" id="name" aria-describedby="yourName" placeholder="Enter Name">
                            <small id="yourName" class="form-text text-muted">Please enter your name.</small>
                        </div>
                        <div class="form-group">
                            <label for="rating">Your Rating</label>
                            <input type="number" class="form-control" name="rating" id="rating" aria-describedby="yourRating" placeholder="Enter Rating">
                            <small id="yourRating" class="form-text text-muted">Rate the product from 1 to 5.</small>
                        </div>
                        <div class="form-group">
                            <label for="review">Review</label>
                            <textarea class="form-control" name="review" id="review" aria-describedby="yourReview" placeholder="Additional Comments"></textarea>
                            <small id="yourReview" class="form-text text-muted">What did you think?</small>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Check In</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include 'template_parts/footer_includes.php' ?>
<!-- Axios -->
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
</body>
</html>
