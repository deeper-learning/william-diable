<?php


class User
{
    public $name = 'Joe Bloggs';
}

$user = new User();

echo $user->name . '<br>'; // Joe Bloggs

$user->name = 'Jane Doe';

echo $user->name; // Jane Doe
