<?php

$result = '';
if (!empty($_POST)) {
    $result = $_POST['a'] + $_POST['b'];
    echo $result;
    die();
}

if (!empty($_GET)) {
    $result = $_GET['a'] + $_GET['b'];
    echo $result;
    die();
}

?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <title>Maths Calculations</title>
</head>
<body>
<div class="container">
    <h1>Maths Fun</h1>
    <form id="form" class="form" method="get">
        <div class="form-inline">
            <div class="form-group">
                <label for="a">A:</label>
                <input type="text" name="a" id="a" class="form-control"> <span>+</span>
            </div>
            <div class="form-group">
                <label for="b">B:</label>
                <input type="text" name="b" id="b" class="form-control"> <span>=</span>
            </div>
            <div class="form-group">
                <label for="result">Result:</label>
                <input type="text" id="result" readonly class="form-control" value="<?= $result;?>">
            </div>
        </div>
        <div class="form-inline">
            <div class="form-group">
                <label for="form-method" class="form-check-label">Post?</label>
                <input type="checkbox" id="form-method" class="form-check-input">
            </div>
            <button type="submit" class="btn btn-primary">Calculate</button>
        </div>
    </form>
</div>
<script>
    window.addEventListener('DOMContentLoaded', function () {
        let form = document.getElementById('form');
        form.addEventListener('submit', function (e) {
            calculate();
            e.preventDefault();
        });
    });

    function calculate() {
        let a = document.getElementById('a');
        let b = document.getElementById('b');
        let result = document.getElementById('result');
        let form_method = document.getElementById('form-method');

        if (!form_method.checked) {
            //GET
            axios.get('maths.php?a=' + a.value + '&b=' + b.value)
                .then(function (response) {
                    console.log(response);
                    result.value = response.data;
                })
                .catch(function (error) {
                    console.log(error);
                });
        } else {
            //POST
            let data = new FormData();
            data.append('a', a.value);
            data.append('b', b.value);

            axios.post('maths.php', data)
                .then(function (response) {
                    console.log(response);
                    result.value = response.data;
                })
                .catch(function (error) {
                    console.log(error);
                });
        }
    }
</script>
</body>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
</html>
