<?php

class ParentClass
{
    private $variable;

    protected function getVariable()
    {
        return $this->variable;
    }

    protected function setVariable($variable)
    {
        $this->variable = strip_tags($variable);
    }

    protected function foo()
    {
        return 'Value from ' . __METHOD__;
    }

    private function bar()
    {
        return 'Value from ' . __METHOD__;
    }
}

class Child extends ParentClass
{
    public function baz()
    {
        echo parent::foo(); // Success
        echo $this->foo(); // Success and equivalent to previous line

        echo parent::bar();

        parent::setVariable('thing');

        echo parent::getVariable();
        echo $this->getVariable(); // equivalent to previous line
    }
}

$child = new Child();
$child->baz();
