<?php

abstract class Duck
{
    abstract public function quack();
}

class Mallard extends Duck
{
    public function quack()
    {
        return 'I quack like a Mallard!';
    }
}

class Redhead extends Duck
{

    public function quack()
    {
        // TODO: Implement quack() method.
    }
}

$mallard = new Mallard();
echo $mallard->quack();
