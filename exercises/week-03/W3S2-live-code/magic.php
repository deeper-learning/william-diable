<?php

class Product
{
    public $title;

    public function __construct($title)
    {
        $this->title = $title;
    }

    public function __serialize()
    {
        return [
            'title' => 'Edited before serialization'
        ];
    }

    public function __toString()
    {
        return 'Title: ' . $this->title;
    }
}

$product = new Product('Macbook Pro');

echo $product . '<br>';

var_dump(serialize($product));
