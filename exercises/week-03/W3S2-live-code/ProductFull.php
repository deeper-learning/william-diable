<?php


class ProductFull
{
    public string $title;
    public float $rating;
    public bool $isReleased;

    public function __construct(string $title, float $rating, bool $isReleased = false)
    {
        $this->title = $title;
        $this->rating = $rating;
        $this->isReleased = $isReleased;
    }

    public function releaseProduct(): void
    {
        $this->isReleased = true;
    }

    public function getDisplayTitle(): string
    {
        $truncatedTitle = $this->truncate($this->title);
        return ucwords($truncatedTitle);
    }

    private function truncate(string $value): string
    {
        return substr($value, 0, 8) . '...';
    }
}

$product = new ProductFull('awesome stuff', 3.5);
var_dump($product->isReleased); // false
$product->releaseProduct();
var_dump($product->isReleased); // true

echo '<br>' . $product->getDisplayTitle();
