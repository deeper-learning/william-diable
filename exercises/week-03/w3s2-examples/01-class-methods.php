<?php

class TestClass
{
  public $title;
  public $description;
  public $datePublished;
}

$instance = new TestClass();
$instance->title = 'My Title';
// ...
$title = $instance->title;

class Product
{
  public $title = 'Macbook Pro';

  public function getFormattedTitle()
  {
    return strtoupper($this->title);
  }
}

$product = new Product();
echo $product->getFormattedTitle(); // 'MACBOOK PRO'
