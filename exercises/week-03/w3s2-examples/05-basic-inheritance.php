<?php

/**
 * Parent class
 */
class Duck
{
  // Properties shared by all children
  public $name;
  public $headColour;
}


/**
 * Child classes
 */
class Mallard extends Duck
{
  public $headColour = 'green/brown';
}

class Redhead extends Duck
{
  public $headColour = 'red';
}


$myDuck = new Mallard();
$myDuck->name = 'Webster';

echo $myDuck->name; // 'Webster'
echo $myDuck->headColour; // 'green/brown'
