<?php

class User
{
  public string $name;
  public int $age;

  public function __construct($name, $age)
  {
    $this->name = $name;
    $this->age = $age;
  }
}

// We don't want to allow false as an age - that isn't an integer...
$user = new User('Joe Bloggs', false);

// ...but it doesn't error

// There are two possible outcomes here depending on the strict_types setting
// if strict types is off:
//   PHP will try to cast "false" to an integer value, the outcome being 0
// if strict types is on:
//   PHP will throw a fatal error:
//   Fatal error: Uncaught TypeError: Argument 1 passed to test() must be of the type int, bool given
echo $user->age; // 0
