<?php

// This may come from, for example, a text field in a form
$dangerousString = 'My Title! <script>alert("HACKED!")</script>';
$escapedString = htmlspecialchars($dangerousString);

echo 'Escaped: ' . $escapedString;
echo '<hr>';
echo 'Unescaped: ' . $dangerousString;
