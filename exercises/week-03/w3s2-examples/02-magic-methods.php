<?php

class Product
{
  public $title;

  public function __construct($title)
  {
    $this->title = $title;
  }

  public function __serialize()
  {
    return [
      'title' => 'Edited before serialisation',
    ];
  }

  public function __toString()
  {
    return 'Title: ' . $this->title;
  }
}

$product = new Product('Macbook Pro');

// We treat the object as a string, therefore using __toString()
echo $product . '<br>'; // 'Title: Macbook Pro'

var_dump(serialize($product));
