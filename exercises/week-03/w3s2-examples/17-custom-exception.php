<?php

class AuthenticationException extends Exception
{
}

class TestClass
{
  public function doThing()
  {
    $userIsAuthenticated = false;

    if (!$userIsAuthenticated) {
      throw new AuthenticationException('You must be logged in to do that!');
    }
  }
}

try {
  $object = new TestClass();
  $object->doThing()
} catch (AuthenticationException $e) {
  // An AuthenticationException specifically was caught
} catch (Exception $e) {
  // Any other Exception thrown by the code within try{} was caught
}
