<?php

abstract class Duck
{
  public $name;
  public $headColour;
}

class Mallard extends Duck {
  public $headColour = 'green/brown';
}

// Successful!
$myDuck = new Mallard();

// Fatal error: Uncaught Error: Cannot instantiate abstract class Duck
$badDuck = new Duck();
