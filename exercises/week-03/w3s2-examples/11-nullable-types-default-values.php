<?php

class TestClass
{
  public function nullableParameter(?string $nullable)
  {
    echo $nullable; // Can be any string, or null
  }

  public function defaultedParameter(string $defaulted = 'Hello World')
  {
    echo $defaulted;
  }
}

$object = new TestClass();
$object->nullableParameter(null); // null
$object->nullableParameter('A string this time'); // 'A string this time'

$object->defaultedParameter(); // 'Hello World'
$object->defaultedParameter('I provided my own'); // 'I provided my own'
