<?php

class User
{
  public string $name;
  public int $age;

  public function __construct($name, $age)
  {
    $this->name = $name;
    $this->age = $age;
  }
}

// Success!
$user = new User('Joe Bloggs', 50);

// Fatal error: Uncaught TypeError: Typed property User::$age must be int,
// string used
$user2 = new User('Jane Doe', 'invalid');
