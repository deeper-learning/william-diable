<?php

class TestClass
{
  public function doThing()
  {
    $userIsAuthenticated = false;

    if (!$userIsAuthenticated) {
      throw new Exception('You must be logged in to do that!');
    }
  }
}

try {
  $object = new TestClass();
  $object->doThing()
} catch (Exception $e) {
  // Some error handling here
}
