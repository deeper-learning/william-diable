<?php

// Note: Properties work the same way as methods
class ParentClass
{
  protected function foo()
  {
    return 'Value from ParentClass::foo()';
  }

  private function bar()
  {
    return 'Value from ParentClass::bar()';
  }
}

class Child extends ParentClass
{
  public function baz()
  {
    // Success!
    echo parent::foo();

    // Fatal error: Uncaught Error: Call to private method ParentClass::bar()
    // from context 'Child'
    echo parent::bar();
  }
}

$child = new Child();
$child->baz();
