<?php

abstract class Duck
{
  abstract public function quack();
}

class Mallard extends Duck
{
  public function quack()
  {
    return 'I quack like a Mallard!';
  }
}

class Redhead extends Duck {
  // The "quack" method is not defined so we get an error;

  // Fatal error: Class Redhead contains 1 abstract method and must therefore
  // be declared abstract or implement the remaining methods (Duck::quack)
}

$mallard = new Mallard();
echo $mallard->quack(); // 'I quack like a Mallard!'
