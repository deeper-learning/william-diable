<?php

class User
{
  public string $name;
  public DateTime $dateOfBirth;

  public function __construct(string $name, DateTime $dateOfBirth)
  {
    $this->name = $name;
    $this->dateOfBirth = $dateOfBirth;
  }
}

// Success!
$dateOfBirth = new DateTime('1990-05-20');
$user = new User('Joe Bloggs', $dateOfBirth);

// Again, PHP will cast 9 to a string of '9'
// But it cannot cast a string to an object instance, so we get a Fatal;

// Fatal error: Uncaught TypeError: Argument 2 passed to User::__construct()
// must be an instance of DateTime, string given
$user2 = new User(9, 'hello');
