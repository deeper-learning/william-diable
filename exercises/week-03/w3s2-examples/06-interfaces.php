<?php

interface Duck
{
  public function quack();
}

class Mallard implements Duck
{
  public function quack()
  {
    echo 'Mallard quack!';
  }
}

class Redhead implements Duck
{
  // Fatal error: Class Redhead contains 1 abstract method and must therefore
  // be declared abstract or implement the remaining methods (Duck::quack)
}
