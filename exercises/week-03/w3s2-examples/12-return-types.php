<?php

class TestClass
{
  public function stringReturnType(): string
  {
    return 'I am a string!';
  }

  public function nullableReturnType(): ?string
  {
    return null;
  }

  public function incorrectReturnType(): int
  {
    return 'I am not an integer!';
  }
}

$object = new TestClass();

echo $object->stringReturnType(); // 'I am a string!'
echo $object->nullableReturnType(); // null

// Fatal error: Uncaught TypeError: Return value of
// TestClass::incorrectReturnType() must be of the type int, string returned
echo $object->incorrectReturnType();
