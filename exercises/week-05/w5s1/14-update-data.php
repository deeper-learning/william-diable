<?php

require_once 'db.php';

// We're pretending this comes from user input, e.g. $_POST['id']
$productId = 9;
$productTitle = 'Updated Product Title';

$stmt = $dbh->prepare('UPDATE product set title = :title WHERE id = :id');

$stmt->execute([
    'id' => $productId,
    'title' => $productTitle,
]);

// We can optionally check how many rows were affected by the statement
echo '# Rows affected: ' . $stmt->rowCount(); // 1
