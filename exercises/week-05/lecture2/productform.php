<?php

require_once 'db.php';

$productAddedSuccess = false;

if (!empty($_POST)) {
    //var_dump($_POST);
    $newProductTitle = $_POST['title'];

    $stmt = $dbh->prepare('INSERT INTO product (title) VALUES (:title)');

    $stmt->execute([
        'title' => $newProductTitle
    ]);

    $productAddedSuccess = true;
}
?>


<!doctype html>
<html>
<head lang="en">
    <title>Product Form</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
<body class="container pt-4">
    <form action="" method="post">
        <?php if ($productAddedSuccess): ?>
        <p class="alert alert-success">Product added successfully!</p>
        <?php endif; ?>

        <div class="form-group">
            <label for="product-title">Title:</label>
            <input type="text" name="title" id="product-title" class="form-control">
        </div>

        <button type="submit" class="btn btn-success">Save Product</button>
    </form>

    <script>
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>

</body>
</html>
