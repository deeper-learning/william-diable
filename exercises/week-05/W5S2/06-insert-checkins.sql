-- File: ~/projects/W5S2/06-insert-checkins.sql

INSERT INTO `checkin` (`product_id`, `name`, `rating`, `review`)
  VALUES(1, 'John Smith', 2, 'Not keen on this');

INSERT INTO `checkin` (`product_id`, `name`, `rating`, `review`)
  VALUES(1, 'Jane Doe', 5, 'Love it!');
