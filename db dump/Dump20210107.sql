-- MySQL dump 10.13  Distrib 8.0.20, for macos10.15 (x86_64)
--
-- Host: 127.0.0.1    Database: project
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `project`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `project` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `project`;

--
-- Table structure for table `Animal`
--

DROP TABLE IF EXISTS `Animal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Animal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(12) NOT NULL,
  `img_path` varchar(24) NOT NULL,
  `wiki_path` varchar(24) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `trophic` varchar(16) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `genus` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Animal`
--

LOCK TABLES `Animal` WRITE;
/*!40000 ALTER TABLE `Animal` DISABLE KEYS */;
INSERT INTO `Animal` VALUES (1,'Indian Stick','Users/user/projects/deep','https://en.wikipedia.org','The common, Indian or laboratory stick insect is a species of Phasmatodea (phasmid) that is often kept by schools and individuals as pets',NULL,NULL,NULL),(2,'Fennec fox','Users/user/projects/deep','https://en.wikipedia.org','The fennec fox is a small crepuscular fox native to the Sahara Desert and the Sinai Peninsula. Its most distinctive feature is its unusually large ears, which serve to dissipate heat',NULL,NULL,NULL),(3,'Suri alpaca','Users/user/projects/deep','https://en.wikipedia.org','Suri alpaca is one of two breeds of the alpaca, the other being the Huacaya. Of 3.7 million alpacas worldwide, less than 10% are thought to be of the Suri breed',NULL,NULL,NULL),(4,'Tibetan Mast','Users/user/projects/deep','https://en.wikipedia.org','The Tibetan Mastiff is a large Tibetan dog breed belonging to the mastiff family. Its double coat is long, subject to climate, and found in a wide variety of colors',NULL,NULL,NULL),(5,'Bengal Tiger','Users/user/projects/deep','https://en.wikipedia.org','The Bengal tiger is a tiger from a specific population of the Panthera tigris tigris subspecies that is native to the Indian subcontinent. It is threatened by poaching, loss, and fragmentation of habitat',NULL,NULL,NULL),(6,'African Pygm','Users/user/projects/deep','https://en.wikipedia.org','The domesticated hedgehog kept as a pet is typically the African pygmy hedgehog. Other species kept as pets include the Egyptian long-eared hedgehog and the Indian long-eared hedgehog',NULL,NULL,NULL),(7,'Honduran Mil','Users/user/projects/deep','https://en.wikipedia.org','The Honduran Milk Snake is the most popular species of the Milksnake family. They can make very good pets although not quite as popular as Corn Snakes or Royal Pythons for beginners as they will often “musk” on you if scared, and as hatchlings are much mo',NULL,NULL,NULL),(8,'Chinese Wate','Users/user/projects/deep','https://en.wikipedia.org','Chinese water dragons (AKA green water dragons) are exotic pets that are not for beginners. They need to have their food and heating needs accurately monitored to flourish',NULL,NULL,NULL),(9,'Red Panda','Users/user/projects/deep','https://en.wikipedia.org','Everybody loves red pandas, they’re cute, fluffy, unique and endearing. They are also unfortunately very endangered. No one knows exactly how many red pandas exist in the wild today',NULL,NULL,NULL),(10,'Alligator','Users/user/projects/deep','https://en.wikipedia.org','An alligator is a crocodilian in the genus Alligator of the family Alligatoridae. The two extant species are the American alligator and the Chinese alligator',NULL,NULL,NULL);
/*!40000 ALTER TABLE `Animal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `checkin`
--

DROP TABLE IF EXISTS `checkin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `checkin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `animal_id` int(11) DEFAULT NULL,
  `user_name` varchar(16) NOT NULL,
  `rating` int(100) NOT NULL,
  `review` varchar(255) NOT NULL,
  `posted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `checkin`
--

LOCK TABLES `checkin` WRITE;
/*!40000 ALTER TABLE `checkin` DISABLE KEYS */;
INSERT INTO `checkin` VALUES (1,1,'testing',5,'pah!','2021-01-05 22:23:45'),(2,2,'Charlie',3,'Ate the postman','2021-01-05 22:55:09'),(3,3,'Bob',2,'testing please','2021-01-06 23:27:23'),(4,4,'Willy Wonka',1,'Bit my fingers off!','2021-01-06 23:28:03');
/*!40000 ALTER TABLE `checkin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `lecture`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `lecture` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `lecture`;

--
-- Table structure for table `checkin`
--

DROP TABLE IF EXISTS `checkin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `checkin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `review` varchar(500) DEFAULT NULL,
  `posted` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `checkin`
--

LOCK TABLES `checkin` WRITE;
/*!40000 ALTER TABLE `checkin` DISABLE KEYS */;
INSERT INTO `checkin` VALUES (1,1,'John Smith',2,'Not keen on this','2020-12-04 21:36:15'),(2,1,'Jane Doe',5,'Love it!','2020-12-04 21:36:15'),(3,NULL,'Charlie Chaplin',3,'Please work','2020-12-28 18:58:51'),(4,NULL,'Kramer',1,'I\'m out!','2020-12-28 19:02:58'),(5,NULL,'Alan Bennett',5,'A-maze-balls','2020-12-29 22:54:21'),(6,NULL,'Bob',2,'Meh','2020-12-29 22:55:09'),(7,NULL,'Bob2',2,'Still meh','2020-12-29 22:55:21'),(8,NULL,'Me',3,'Testing product ID review. Please work','2020-12-29 22:58:14'),(9,NULL,'',0,'','2020-12-30 00:17:01'),(10,NULL,'',0,'','2020-12-30 00:40:26'),(11,NULL,'testing site',5,'hello\r\n','2021-01-05 21:19:55'),(12,NULL,'tsting',4,'meow','2021-01-05 21:48:52'),(13,NULL,'tsting',4,'meow','2021-01-05 21:48:57'),(14,NULL,'tsting',4,'meow','2021-01-05 21:49:00'),(15,NULL,'tsting',4,'meow','2021-01-05 21:49:03'),(16,NULL,'testing replaceState',1,';lkasjdf','2021-01-05 21:59:14'),(17,NULL,'testing replaceState',1,';lkasjdf','2021-01-05 21:59:17'),(18,NULL,'testing replaceState',1,';lkasjdf','2021-01-05 21:59:19'),(19,NULL,'testing replaceState',1,';lkasjdf','2021-01-05 21:59:20'),(20,NULL,'testing replaceState2',1,'pah','2021-01-05 21:59:54'),(21,NULL,'dfg',6,'sfgdh','2021-01-05 22:10:27');
/*!40000 ALTER TABLE `checkin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'Macbook Pro'),(2,'Pencil'),(3,'Ruler'),(4,'Coat'),(5,'Milk'),(6,'testing'),(7,'testing'),(8,'testing product form hello'),(9,'testingagain'),(10,'testingagain'),(11,'testingagain'),(12,'testingagain'),(13,'testingagain'),(14,'testingagain'),(15,'testingagain'),(16,'test-replace-state'),(17,'submit test!'),(18,'Another testing of the input'),(19,'');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `email_address` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_address` (`email_address`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-07  9:45:09
